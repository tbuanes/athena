#include "../SimKernel.h"
#include "../SimKernelMT.h"
#include "../CollectionMerger.h"
#include "../SimHitTreeCreator.h"
#include "../SimEventFilter.h"

DECLARE_COMPONENT( ISF::SimKernel )
DECLARE_COMPONENT( ISF::SimKernelMT )
DECLARE_COMPONENT( ISF::CollectionMerger )
DECLARE_COMPONENT( ISF::SimHitTreeCreator )
DECLARE_COMPONENT( ISF::SimEventFilter )
